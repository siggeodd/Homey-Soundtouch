# Bose Soundtouch v4.0.3

This app adds support for Bose Soundtouch devices for Homey.

This app does NOT work with Sound smart devices since these devices use a new API by Bose. If your Bose device is controlled using the Bose Music app than your device uses the new Smart API and is therefor not compatible.

Features:
* Connecting Soundtouch devices based on IP address
* Play/Pause/Next/Previous
* Setting volume
* Setting Bass using a slider, no more hassle!
* Creating zones
* Trigger for a speaker if it started/stopped playing
* Trigger for volume changes, very handy to keep volume in sync between devices
* Condition if a speaker is playing
* Condition if a speaker is in a zone
* Condition if a speaker is the master of a zone
* Condition if a speaker is in a zone with another specific speaker

For any questions or feature requests, please see the forum. Please only create issues to report bugs.
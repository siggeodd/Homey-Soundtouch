'use strict';

const SoundtouchDriver = require("../soundtouch/soundtouchdriver");

class Soundtouch10Driver extends SoundtouchDriver {
    async onPairListDevices(data, callback) {
        await super.onPairListDevices('SoundTouch 10', data, callback);
    }
}

module.exports = Soundtouch10Driver;
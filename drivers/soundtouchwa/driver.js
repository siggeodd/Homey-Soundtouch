'use strict';

const SoundtouchDriver = require("../soundtouch/soundtouchdriver");

class SoundtouchwaDriver extends SoundtouchDriver {
    async onPairListDevices(data, callback) {
        await super.onPairListDevices('SoundTouch Wireless Adapter', data, callback);
    }
}

module.exports = SoundtouchwaDriver;
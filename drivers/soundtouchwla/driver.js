'use strict';

const SoundtouchDriver = require("../soundtouch/soundtouchdriver");

class SoundtouchwlaDriver extends SoundtouchDriver {
    async onPairListDevices(data, callback) {
        await super.onPairListDevices('SoundTouch Wireless Link Adapter', data, callback);
    }
}

module.exports = SoundtouchwlaDriver;
'use strict';

const SoundtouchDriver = require("../soundtouch/soundtouchdriver");

class Soundtouch20Driver extends SoundtouchDriver {
    async onPairListDevices(data, callback) {
        await super.onPairListDevices('SoundTouch 20', data, callback);
    }
}

module.exports = Soundtouch20Driver;
'use strict';

const SoundtouchDriver = require("../soundtouch/soundtouchdriver");

class Soundtouch30Driver extends SoundtouchDriver {
    async onPairListDevices(data, callback) {
        await super.onPairListDevices('SoundTouch 30', data, callback);
    }
}

module.exports = Soundtouch30Driver;
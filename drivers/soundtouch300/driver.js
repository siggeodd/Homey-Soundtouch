'use strict';

const SoundtouchDriver = require("../soundtouch/soundtouchdriver");

class Soundtouch300Driver extends SoundtouchDriver {
    async onPairListDevices(data, callback) {
        await super.onPairListDevices('SoundTouch 300', data, callback);
    }
}

module.exports = Soundtouch300Driver;
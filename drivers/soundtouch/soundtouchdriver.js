'use strict';

const Homey = require('homey');
const SoundtouchApi = require("../../api/api.js");

class SoundtouchDriver extends Homey.Driver {
    async onPairListDevices(name, data, callback) {
        const discoveryStrategy = this.getDiscoveryStrategy();
        const discoveryResults = discoveryStrategy.getDiscoveryResults();

        const promises = Object.values(discoveryResults).map(async discoveryResult => {
            const api = new SoundtouchApi(discoveryResult.address, null);
            const info = await api.getInfo();
            if (info.type === name) {
                return {
                    name: info.name,
                    data: {
                        ip: info.ip,
                        mac: info.mac,
                        serial: info.serialNumber
                    }
                };
            }
        });
        const awaitDevices = await Promise.all(promises);
        const devices = awaitDevices.filter(el => {
            return (el !== null) && (el !== undefined);
        });
        callback(null, devices);
    }
}

module.exports = SoundtouchDriver;
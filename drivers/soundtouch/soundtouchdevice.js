'use strict';

const Homey = require('homey');
const SoundtouchApi = require("../../api/api.js");
const WebSocketClient = require('websocket').client;

class SoundtouchDevice extends Homey.Device {

    // this method is called when the Device is inited
    onInit() {
        const device = this;
        this._api = new SoundtouchApi(this.getData().ip, this.getData().mac);
        this._registerCapabilities();

        this._startedPlayingTrigger = new Homey.FlowCardTriggerDevice('started_playing')
            .register();

        this._stoppedPlayingTrigger = new Homey.FlowCardTriggerDevice('stopped_playing')
            .register();

        this._volumeChangedTrigger = new Homey.FlowCardTriggerDevice('changed_volume')
            .register();

        this._albumArt = new Homey.Image('jpg');
        this._albumArt.setUrl(null);
        this._albumArt.register()
            .then(() => {
                return this.setAlbumArtImage(this._albumArt);
            })
            .catch((e) => {
                console.log(e);
            });

        const _setSourceAction = new Homey.FlowCardAction('set_source')
            .register()
            .registerRunListener(async (args, state) => {
                this.log('select', args);
                try {
                    args.device._api.setSource(args.source);
                    return Promise.resolve();
                } catch (e) {
                    return Promise.reject(e);
                }
            });

        const _setShuffleAction = new Homey.FlowCardAction('shuffle')
            .register()
            .registerRunListener(async (args, state) => {
                this.log('shuffle', args);
                try {
                    switch (args.shuffle) {
                        case 'shuffle_on': return Promise.resolve(await args.device._api.shuffle_on());
                        case 'shuffle_off': return Promise.resolve(await args.device._api.shuffle_off());
                    }
                } catch (e) {
                    return Promise.reject(e);
                }
            });

        const _setRepeatAction = new Homey.FlowCardAction('repeat')
            .register()
            .registerRunListener(async (args, state) => {
                this.log('repeat', args);
                try {
                    switch (args.repeat) {
                        case 'repeat_none': return Promise.resolve(await args.device._api.repeat_none());
                        case 'repeat_one': return Promise.resolve(await args.device._api.repeat_one());
                        case 'repeat_off': return Promise.resolve(await args.device._api.repeat_off());
                    }
                } catch (e) {
                    return Promise.reject(e);
                }
            });

        device.startWebSocket().catch(e => {
            console.log(e);
        });
    }

    _registerCapabilities() {
        const capabilitySetMap = new Map([
            ['onoff', this._onOff],
            ['speaker_playing', this._play],
            ['speaker_prev', this._prev],
            ['speaker_next', this._next],
            ['volume_set', this._setVolume],
            ['volume_mute', this._setMute]
        ]);
        this.getCapabilities().forEach(capability =>
        this.registerCapabilityListener(capability, (value) => {
            return capabilitySetMap.get(capability).call(this, value)
                .catch(err => {
                    return Promise.reject(err);
                });
        }))
    }

    async _onOff(state) {
        this.log('onOff', state);
        try {
            const isTurnedOn = await this._api.isOn();
            if (state === true) {
                if (!isTurnedOn) {
                    this._api.power();
                }
            } else {
                if (isTurnedOn) {
                    this._api.power();
                }
            }
        } catch (e) {
            return Promise.reject(e);
        }
    }

    async _play(state) {
        this.log('play', state);
        try {
            if (state === true) {
                return Promise.resolve(await this._api.play());
            } else {
                return Promise.resolve(await this._api.pause());
            }
        } catch (e) {
            return Promise.reject(e);
        }
    }

    async _prev(state) {
        this.log('prev', state);
        try {
            return Promise.resolve(await this._api.prev_track());
        } catch (e) {
            return Promise.reject(e);
        }
    }

    async _next(state) {
        this.log('next', state);
        try {
            return Promise.resolve(await this._api.next_track());
        } catch (e) {
            return Promise.reject(e);
        }
    }

    async _setVolume(volume) {
        this.log('volume', volume);
        try {
            return Promise.resolve(await this._api.setVolume(volume * 100));
        } catch (e) {
            return Promise.reject(e);
        }
    }

    async _setMute(mute) {
        this.log('mute', mute);
        try {
            if (mute) {
                if (!this.getCapabilityValue('volume_mute')) {
                    return Promise.resolve(await this._api.mute());
                }
            } else {
                if (this.getCapabilityValue('volume_mute')) {
                    return Promise.resolve(await this._api.mute());
                }
            }
        } catch (e) {
            return Promise.reject(e);
        }
    }

    async startWebSocket() {
        const device = this;
        const client = new WebSocketClient();
        client.on('connectFailed', (error) => {
            console.log('Connect Error: ' + error.toString());
            device.setUnavailable(error.toString());
        });

        client.on('connect', (connection) => {
            console.log('device connected');
            this.setAvailable();
            connection.on('error', (error) => {
                console.log("Connection Error: " + error.toString());
                device.setUnavailable(error.toString());
                setTimeout(device.startWebSocket, 3000);
            });
            connection.on('close', () => {
                console.log('Connection Closed');
                device.setUnavailable('Connection is closed');
                setTimeout(device.startWebSocket, 3000);
            });
            connection.on('message', async (message) => {
                if (message.type === 'utf8') {
                    // message.utf8Data
                    const xmlMessageParsed = await this._api.parseXML(message.utf8Data).catch(e => {
                        console.log(e);
                    });
                    if (xmlMessageParsed.updates) {
                        console.log(xmlMessageParsed.updates);
                        await this._parseVolumeMessage(xmlMessageParsed.updates);
                        await this._parseNowPlayingMessage(xmlMessageParsed.updates);
                    }
                }
            });
        });
        client.connect(`ws://${this.getData().ip}:8080`, "gabbo");
    }

    async _parseVolumeMessage(message) {
        if (message && message.volumeUpdated) {
            try {
                const volume = parseInt(message.volumeUpdated[0].volume[0].targetvolume[0]) / 100;
                const mute = (message.volumeUpdated[0].volume[0].muteenabled[0] === 'true');
                const token = {
                    'volume': mute ? 0 : volume
                };
                if (this.getCapabilityValue('volume_set') !== volume ||
                    this.getCapabilityValue('volume_mute') !== mute) {
                    this._volumeChangedTrigger.trigger(this, token).catch(e => console.log('vage error'));
                }
                this.setCapabilityValue('volume_set', volume);
                this.setCapabilityValue('volume_mute', mute);
            } catch (e) {
                console.log('ERROR parsing volume message');
                console.log(e);
            }
        }
    }

    async _parseNowPlayingMessage(message) {
        if (message && message.nowPlayingUpdated) {
            try {
                const isTurnedOn = message.nowPlayingUpdated[0].nowPlaying[0].$.source !== 'STANDBY';
                const wasPlaying = this.getCapabilityValue('speaker_playing');
                const isPlaying = isTurnedOn && (message.nowPlayingUpdated[0].nowPlaying[0].playStatus && message.nowPlayingUpdated[0].nowPlaying[0].playStatus[0] !== 'PAUSE_STATE');
                this.setCapabilityValue('onoff', isTurnedOn);
                this.setCapabilityValue('speaker_playing', isPlaying);
                if (message.nowPlayingUpdated[0].nowPlaying[0].time && message.nowPlayingUpdated[0].nowPlaying[0].time[0].$ && message.nowPlayingUpdated[0].nowPlaying[0].time[0].$.total) {
                    this.setCapabilityValue('speaker_duration', parseInt(message.nowPlayingUpdated[0].nowPlaying[0].time[0].$.total));
                    this.setCapabilityValue('speaker_position', parseInt(message.nowPlayingUpdated[0].nowPlaying[0].time[0]._));
                }

                if (wasPlaying === false && isPlaying === true) {
                    this._startedPlayingTrigger.trigger(this, null).catch(e => console.log(e));
                }
                if (wasPlaying === true && isPlaying === false) {
                    this._stoppedPlayingTrigger.trigger(this, null).catch(e => console.log(e));
                }

                if (message.nowPlayingUpdated[0].nowPlaying[0].track && message.nowPlayingUpdated[0].nowPlaying[0].track[0]) {
                    this.setCapabilityValue('speaker_track', message.nowPlayingUpdated[0].nowPlaying[0].track[0]);
                } else {
                    this.setCapabilityValue('speaker_track', null);
                }
                if (message.nowPlayingUpdated[0].nowPlaying[0].artist && message.nowPlayingUpdated[0].nowPlaying[0].artist[0]) {
                    this.setCapabilityValue('speaker_artist', message.nowPlayingUpdated[0].nowPlaying[0].artist[0]);
                } else {
                    this.setCapabilityValue('speaker_artist', null);
                }
                if (message.nowPlayingUpdated[0].nowPlaying[0].album && message.nowPlayingUpdated[0].nowPlaying[0].album[0]) {
                    this.setCapabilityValue('speaker_album', message.nowPlayingUpdated[0].nowPlaying[0].album[0]);
                } else {
                    this.setCapabilityValue('speaker_album', null);
                }
                if (message.nowPlayingUpdated[0].nowPlaying[0].art && message.nowPlayingUpdated[0].nowPlaying[0].art[0].$.artImageStatus === 'IMAGE_PRESENT') {
                    const url = message.nowPlayingUpdated[0].nowPlaying[0].art[0]._.replace(/^http:\/\//i, 'https://');
                    if (this._albumArt._source !== url) {
                        this._albumArt.setUrl(url);
                        this._albumArt.update();
                    }
                } else {
                    if (this._albumArt._source !== null) {
                        this._albumArt.setUrl(null);
                        this._albumArt.update();
                    }
                }
            } catch (e) {
                console.log(e);
                return e;
            }
        }
    }
}

module.exports = SoundtouchDevice;
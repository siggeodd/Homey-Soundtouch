'use strict';

const Homey = require('homey');

class Soundtouch extends Homey.App {
	
	onInit() {
		this.log('app is running');

        const _isPlayingCondition = new Homey.FlowCardCondition('is_playing')
            .register()
            .registerRunListener((args, state) => {
                return Promise.resolve(args.device.getCapabilityValue('speaker_playing'));
            });

        const _isInZoneCondition = new Homey.FlowCardCondition('is_in_zone')
            .register()
            .registerRunListener(async (args, state) => {
                try {
                    return Promise.resolve(await args.device._api.isInZone());
                } catch (e) {
                    return Promise.reject(e);
                }
            });

        const _isInZoneWithCondition = new Homey.FlowCardCondition('is_in_zone_with')
            .register()
            .registerRunListener(async (args, state) => {
                try {
                    return Promise.resolve(await args.device._api.isInZoneWith(args.sibling));
                } catch (e) {
                    return Promise.reject(e);
                }
            });

        const _isMasterInZoneCondition = new Homey.FlowCardCondition('is_zone_master')
            .register()
            .registerRunListener(async (args, state) => {
                try {
                    return Promise.resolve(await args.device._api.isZoneMaster());
                } catch (e) {
                    return Promise.reject(e);
                }
            });

        const _playPresetAction = new Homey.FlowCardAction('play_preset')
            .register()
            .registerRunListener(async (args, state) => {
                this.log('preset', args.device);
                try {
                    switch (parseInt(args.preset_number)) {
                        case 1: return Promise.resolve(await args.device._api.preset_1());
                        case 2: return Promise.resolve(await args.device._api.preset_2());
                        case 3: return Promise.resolve(await args.device._api.preset_3());
                        case 4: return Promise.resolve(await args.device._api.preset_4());
                        case 5: return Promise.resolve(await args.device._api.preset_5());
                        case 6: return Promise.resolve(await args.device._api.preset_6());
                    }
                } catch (e) {
                    return Promise.reject(e);
                }
            });

        const _setBassCapability = new Homey.FlowCardAction('bass_capability')
            .register()
            .registerRunListener(async (args, state) => {
                this.log('bass', args);
                try {
                    await args.device._api.setBassPercentage(args.bass_number * 100);
                    return Promise.resolve(true);
                } catch (e) {
                    return Promise.reject(e);
                }
            });

        const _createZoneWithAction = new Homey.FlowCardAction('create_zone_with')
            .register()
            .registerRunListener(async (args, state) => {
                //this.log('create zone', args);
                try {
                    console.log('creating zone started');
                    this.log('slave ip: ', args.slave.getData().ip);
                    this.log('slave mac: ', args.slave.getData().mac);
                    await args.device._api.createZone(args.slave.getData().ip, args.slave.getData().mac);
                    return Promise.resolve(true);
                } catch (e) {
                    return promise.reject(e);
                }
            });

        const _addSlaveToZoneAction = new Homey.FlowCardAction('add_slave_to_zone')
            .register()
            .registerRunListener(async (args, state) => {
                this.log('add slave', args);
                try {
                    await args.device._api.addSlaveToZone(args.slave.getData().ip, args.slave.getData().mac);
                    return Promise.resolve(true);
                } catch (e) {
                    return Promise.reject(e);
                }
            });

        const _removeSlaveFromZoneAction = new Homey.FlowCardAction('remove_slave_from_zone')
            .register()
            .registerRunListener(async (args, state) => {
                this.log('remove slave', args);
                try {
                    await args.device._api.removeFromZone(args.slave.getData().ip, args.slave.getData().mac);
                    return Promise.resolve();
                } catch (e) {
                    return Promise.reject(e);
                }
            });
	}
}

module.exports = Soundtouch;